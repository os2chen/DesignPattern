package com.eugene.designpattern.adapter;

/**
 * 对象适配器，内部持有被适配对象，更加灵活，通过继承Target，使陪适配对象转化成Target类型的
 * <p>
 * 对象适配器使用组合的方式实现适配，被适配对象可以是不同的子类，更加灵活
 * <p>
 * ListView的适配器就是对象适配器，被适配对象为itemView
 * <p>
 * 适配器和装饰者的区别，适配器将一种接口转换成另一种接口，实现了两种接口的转换
 * 装饰者是对目标接口进行包装和扩展
 */

public class ObjectAdapter implements Target {

    private ObjectAdaptee objectAdaptee;

    public ObjectAdapter(ObjectAdaptee objectAdaptee) {
        this.objectAdaptee = objectAdaptee;
    }

    public int getVolt220() {
        return objectAdaptee.getVolt220();
    }

    @Override
    public int getVolt5() {
        return 5;
    }
}
