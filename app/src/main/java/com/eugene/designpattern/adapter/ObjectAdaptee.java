package com.eugene.designpattern.adapter;

/**
 * 对象适配器
 * <p>
 * 被适配对象
 */

public class ObjectAdaptee {

    /**
     * 输出220V电压
     *
     * @return
     */
    public int getVolt220() {
        return 220;
    }


}
