package com.eugene.designpattern.adapter;

/**
 * 类适配器，不如对象适配器灵活
 * <p>
 * 希望通过适配器将220V转化成5V电压，但是还是Target类型
 * <p>
 * 通过多重继承目标接口和被适配者类方式来实现适配
 * 多重继承，其中继承的目标接口的部分来达到适配的目的，继承被适配者类的部分来达到通过调用被适配者
 * 类里面的方法来实现目标接口的功能
 */

public class ClassAdapter extends ClassAdaptee implements Target {

    @Override
    public int getVolt5() {
        return 5;
    }

}
