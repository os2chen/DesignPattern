package com.eugene.designpattern.adapter;

/**
 * 目标接口，也就是希望通过适配器转换得到的接口
 */
public interface Target {

    /**
     * 输出5V电压
     */
    int getVolt5();

}
