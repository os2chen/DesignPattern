package com.eugene.designpattern.adapter;

/**
 * 类适配器
 * <p>
 * 被适配的类
 */
public class ClassAdaptee {

    /**
     * 输出220V电压
     *
     * @return
     */
    public int getVolt220() {
        return 220;
    }

}
