package com.eugene.designpattern.chain;

/**
 * 抽象处理者
 */

public abstract class AbstractHandler {

    /**
     * 指向下一个节点上的处理对象
     */
    protected AbstractHandler nextHandler;

    private String name;

    public AbstractHandler(String name) {
        this.name = name;
    }

    /**
     * 检查处理者是否有权限处理请求
     *
     * @param request
     */
    protected final void checkRequest(AbstractRequest request) {
        //判断处理者是否有权限处理当前请求
        if (getHandleLevel() == request.getRequestLevel()) {
            handleRequest(request);
        } else {
            //否则将请求抛给下一个处理对象
            if (nextHandler != null) {
                //下一个处理对象继续检查是否有权限处理请求
                nextHandler.checkRequest(request);
            } else {
                //没有对象处理时
                System.out.println("All of handler can not handle the request");
            }
        }
    }

    /**
     * 获取处理者处理请求的权限，权限相同才处理请求
     *
     * @return
     */
    protected abstract int getHandleLevel();

    /**
     * 处理请求
     *
     * @param request 请求对象
     */
    protected abstract void handleRequest(AbstractRequest request);

    public void setNextHandler(AbstractHandler nextHandler) {
        this.nextHandler = nextHandler;
    }


}
