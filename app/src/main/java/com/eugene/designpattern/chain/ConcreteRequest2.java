package com.eugene.designpattern.chain;

/**
 * 具体请求2
 */

public class ConcreteRequest2 extends AbstractRequest {
    public ConcreteRequest2(String name) {
        super(name);
    }

    @Override
    public int getRequestLevel() {
        return 2;
    }
}
