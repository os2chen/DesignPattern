package com.eugene.designpattern.chain;

/**
 * Created by eugene on 2017/2/11.
 */

public class ConcreteHandler1 extends AbstractHandler {

    public ConcreteHandler1(String name) {
        super(name);
    }

    /**
     * 处理权限设为1
     *
     * @return
     */
    @Override
    protected int getHandleLevel() {
        return 1;
    }

    @Override
    protected void handleRequest(AbstractRequest request) {
        //打印Handler1能够处理的请求级别
        System.out.println("Handler1 handle request:" + request.getName()
                + ",the level:" + request.getRequestLevel());
    }
}
