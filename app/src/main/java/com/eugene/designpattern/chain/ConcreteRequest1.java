package com.eugene.designpattern.chain;

/**
 * 具体请求1
 */

public class ConcreteRequest1 extends AbstractRequest {
    public ConcreteRequest1(String name) {
        super(name);
    }

    @Override
    public int getRequestLevel() {
        return 1;
    }
}
