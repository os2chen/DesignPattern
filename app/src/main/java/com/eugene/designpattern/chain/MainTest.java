package com.eugene.designpattern.chain;

/**
 * 测试
 */

public class MainTest {

    public static void main(String[] args) {
        AbstractHandler concreteHandler1 = new ConcreteHandler1("处理者1");
        AbstractHandler concreteHandler2 = new ConcreteHandler2("处理者2");

        //设置下一个处理者
        concreteHandler1.setNextHandler(concreteHandler2);

        AbstractRequest concreteRequest1 = new ConcreteRequest1("请求1");
        AbstractRequest concreteRequest2 = new ConcreteRequest2("请求2");

        //依次处理请求1
        concreteHandler1.handleRequest(concreteRequest1);
        //依次处理请求2
        concreteHandler1.handleRequest(concreteRequest2);
    }

}
