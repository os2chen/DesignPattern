package com.eugene.designpattern.chain;

/**
 * 具体处理者2
 */

public class ConcreteHandler2 extends AbstractHandler {

    public ConcreteHandler2(String name) {
        super(name);
    }

    /**
     * 处理权限设为2
     *
     * @return
     */
    @Override
    protected int getHandleLevel() {
        return 2;
    }

    @Override
    protected void handleRequest(AbstractRequest request) {
        //打印Handler2能够处理的请求级别
        System.out.println("Handler1 handle request:" + request.getName()
                + ",the level:" + request.getRequestLevel());
    }
}
