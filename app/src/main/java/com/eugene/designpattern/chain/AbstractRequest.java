package com.eugene.designpattern.chain;

import java.security.PublicKey;

/**
 * 抽象请求
 */

public abstract class AbstractRequest {

    private String name;

    public AbstractRequest(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    /**
     * 请求级别
     *
     * @return
     */
    public abstract int getRequestLevel();

}
