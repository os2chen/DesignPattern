package com.eugene.designpattern.flyweight;

import java.util.HashMap;
import java.util.Map;

/**
 * 管理所要创建的享元对象
 * 一般容器为map，一般外部状态作为key，享元对象作为值
 */

public class PersonFactory {

    private Map<Object, Person> map = new HashMap<>();

    /**
     * 这里把person的name,age,height一起作为享元对象的key
     * 只有name,age,height都相同时，则认为时同一个person
     *
     * @param name
     * @param age
     * @param height
     * @return
     */
    public Person getPerson(String name, int age, int height) {
        String key = name + age + height;
        if (map.containsKey(key)) {
            return map.get(key);
        } else {
            return new Person(name, age, height);
        }
    }


}
