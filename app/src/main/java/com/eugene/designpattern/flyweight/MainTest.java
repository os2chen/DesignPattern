package com.eugene.designpattern.flyweight;

public class MainTest {

    public static void main(String[] args) {
        PersonFactory factory = new PersonFactory();
        Person person = factory.getPerson("小明", 12, 170);
        person.introduce();

    }
}
