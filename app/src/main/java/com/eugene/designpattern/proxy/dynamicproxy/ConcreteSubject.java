package com.eugene.designpattern.proxy.dynamicproxy;

/**
 * 具体主题类
 */
public class ConcreteSubject implements Subject {

    @Override
    public void request() {
        System.out.println("具体主题类");
    }

}
