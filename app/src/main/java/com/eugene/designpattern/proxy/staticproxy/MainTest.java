package com.eugene.designpattern.proxy.staticproxy;

public class MainTest {

    public static void main(String[] args) {

        IRent reenter = new Renter();

        IRent agency = new Agency(reenter);

        agency.rent();

    }

}
