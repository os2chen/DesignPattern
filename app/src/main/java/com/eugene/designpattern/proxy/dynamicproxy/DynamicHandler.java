package com.eugene.designpattern.proxy.dynamicproxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * 动态代理Handler实现InvocationHandler接口，重写invoke方法
 */
public class DynamicHandler implements InvocationHandler {

    /**
     * 被代理的类,可以代理任意对象
     */
    private Object obj;

    public DynamicHandler(Object obj) {
        this.obj = obj;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

        //TODO 添加代理对象的逻辑

        //通反射的方式来调用被代理对象的方法
        return method.invoke(obj, args);
    }

}
