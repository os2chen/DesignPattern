package com.eugene.designpattern.proxy.staticproxy;

/**
 * 租客
 */
public class Renter implements IRent {

    @Override
    public void rent() {
        System.out.println("我要租房子");
    }

}
