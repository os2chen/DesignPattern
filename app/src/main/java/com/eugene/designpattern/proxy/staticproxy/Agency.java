package com.eugene.designpattern.proxy.staticproxy;

/**
 * 中介
 */
public class Agency implements IRent{

    private IRent renter;

    public Agency(IRent renter) {
        this.renter = renter;
    }

    @Override
    public void rent() {

        System.out.println("我帮客户租房子");

        renter.rent();

        //TODO 处理租房的其他逻辑，如查找房源

    }

}
