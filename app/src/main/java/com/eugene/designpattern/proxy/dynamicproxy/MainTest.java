package com.eugene.designpattern.proxy.dynamicproxy;

import java.lang.reflect.Proxy;

/**
 * 测试
 */
public class MainTest {

    public static void main(String[] args) {

        ConcreteSubject concreteSubject = new ConcreteSubject();

        DynamicHandler dynamicHandler = new DynamicHandler(concreteSubject);
        Class<?> cls = concreteSubject.getClass();

        /**
         * 构造一个Subject的代理对象
         * 参数：
         *      类加载器；
         *      concreteSubject实现的接口
         *      动态代理Handler对象
         */
        Subject subjectProxy = (Subject) Proxy.newProxyInstance(cls.getClassLoader(), cls.getInterfaces(), dynamicHandler);
        subjectProxy.request();
    }

}
