package com.eugene.designpattern.proxy.dynamicproxy;

/**
 * 主题接口
 */
public interface Subject {

    void request();

}
