package com.eugene.designpattern.proxy.staticproxy;

/**
 * 租房行为接口
 */
public interface IRent {

    void rent();

}
