package com.eugene.designpattern.singleton;

/**
 * 静态类部类模式
 */
public class SingleTon03 {

    private SingleTon03() {
    }

    private static class SingletonHolder {
        private static final SingleTon03 instance = new SingleTon03();
    }

    public static SingleTon03 getInstance() {
        return SingletonHolder.instance;
    }

}
