package com.eugene.designpattern.singleton;

import java.util.HashMap;
import java.util.Map;

/**
 * 利用容器实现单例
 */
public class SingleTon05 {

    private static Map<String, Object> objMap = new HashMap<>();

    private SingleTon05() {}

    public void addObj(String key, Object instance) {
        if (!objMap.containsKey(key)) {
            objMap.put(key, instance);
        }
    }

    public static Object getObj(String key) {
        return objMap.get(key);
    }

}
