package com.eugene.designpattern.singleton;

/**
 * Double Check Lock(DCL)双锁检测
 */
public class SingleTon02 {

    private volatile static SingleTon02 instance;

    /**
     * 构造方法私有化，不允许外部创建对象
     */
    private SingleTon02() {
    }

    public static SingleTon02 getInstance() {
        if (instance == null) {//第一次判空避免不必要的同步
            synchronized (SingleTon02.class) {
                if (instance == null) {//第二次判空为了在null的情况下创建实例
                    instance = new SingleTon02();
                }
            }
        }
        return instance;
    }

}
