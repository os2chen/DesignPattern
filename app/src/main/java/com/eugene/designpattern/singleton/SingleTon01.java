package com.eugene.designpattern.singleton;

/**
 * 懒汉模式
 */
public class SingleTon01 {

    /**
     * 在是使用的时候才进行实例化
     */
    private static SingleTon01 instance;

    private SingleTon01() {
    }

    /**
     * 每次调用都进行了同步，造成不必要的同步开销
     * @return
     */
    public static synchronized SingleTon01 getInstance() {
        if (instance == null) {
            instance = new SingleTon01();
        }
        return instance;
    }

}
