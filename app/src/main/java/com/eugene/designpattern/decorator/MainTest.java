package com.eugene.designpattern.decorator;

public class MainTest {

    public static void main(String[] args) {

        //具体被装饰对象
        Component component = new ConcreteComponent();

        Decorator concreteDecoratorA = new ConcreteDecoratorA(component);
        concreteDecoratorA.operate();

    }

}
