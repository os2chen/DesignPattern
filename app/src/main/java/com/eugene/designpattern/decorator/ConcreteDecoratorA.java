package com.eugene.designpattern.decorator;

/**
 * 具体的装饰者
 */
public class ConcreteDecoratorA extends Decorator {

    /**
     * @param component 传入被装饰的具体Component对象
     */
    public ConcreteDecoratorA(Component component) {
        super(component);
    }

    @Override
    public void operate() {
        //装饰方法A和B既可在父类方法调用之前调用也可以在之后调用
        operateA();
        super.operate();
        operateB();
    }

    /**
     * 自定义装饰方法A
     */
    private void operateA() {
        //装饰逻辑A
    }

    /**
     * 自定义装饰方法B
     */
    private void operateB() {
        //装饰逻辑B
    }

}
