package com.eugene.designpattern.decorator;

/**
 * 抽象组件，所有的装饰者都是Component类型
 */
public interface Component {

    void operate();

}
