package com.eugene.designpattern.decorator;

/**
 * 抽象装饰者
 * 其内部一定要有一个指向组件对象的引用，在大多数情况下，该类为抽象类，需要根据不同的装饰逻辑实现不同的具体子类
 * 如果装饰逻辑单一，只有一个的情况下可以省略该类直接作为具体的装饰者
 */
public abstract class Decorator implements Component {

    private Component component;

    /**
     * @param component 传入被装饰的具体Component对象
     */
    public Decorator(Component component) {
        this.component = component;
    }

    @Override
    public void operate() {
        component.operate();
    }

}
