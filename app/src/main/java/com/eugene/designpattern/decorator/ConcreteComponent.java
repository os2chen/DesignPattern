package com.eugene.designpattern.decorator;

/**
 * 具体组件，被装饰的对象
 */
public class ConcreteComponent implements Component {

    @Override
    public void operate() {
        System.out.println("ConcreteComponent operate");
    }

}
