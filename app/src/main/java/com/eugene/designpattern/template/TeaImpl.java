package com.eugene.designpattern.template;

/**
 * 冲泡茶
 */
public class TeaImpl extends AbstractHotDrink {
    @Override
    protected void brew() {
        System.out.println("茶");
    }

    @Override
    protected void addCondiment() {

    }

    @Override
    protected boolean isCondimentHook() {
        return false;
    }
}
