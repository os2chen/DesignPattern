package com.eugene.designpattern.template;

/**
 * 模板模式：封装算法框架，确定了算法的执行顺序，某些特定的算法由特定子类实现
 *
 * 定义了冲泡饮料的框架
 */
public abstract class AbstractHotDrink {

    /**
     * 冲泡热饮
     * 封装冲泡的算法步骤
     */
    protected final void prepareDrink() {
        boilWater();
        brew();
        pourIntoCup();
        //使算法框架不固定，更加灵活
        if (isCondimentHook()) {
            addCondiment();
        } else {
            System.out.println("不加调料");
        }
    }

    protected void boilWater() {
        System.out.println("烧水");
    }

    /**
     * 冲泡具体的饮料
     */
    protected abstract void brew();

    /**
     * 倒入杯子理
     */
    protected void pourIntoCup() {
        System.out.println("倒入杯子");
    }

    /**
     * 加入什么调料
     */
    protected abstract void addCondiment();

    /**
     * 是否加入调料
     *
     * @return
     */
    protected boolean isCondimentHook() {
        return true;
    }
}
