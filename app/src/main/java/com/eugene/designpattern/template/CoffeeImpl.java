package com.eugene.designpattern.template;

/**
 * 冲泡咖啡
 */
public class CoffeeImpl extends AbstractHotDrink {
    @Override
    protected void brew() {
        System.out.println("咖啡");
    }

    @Override
    protected void addCondiment() {
        System.out.println("加牛奶");
    }

}
