package com.eugene.designpattern.simplefactory;

/**
 * 水果接口
 */

public interface IFruit {

    void get();

}
