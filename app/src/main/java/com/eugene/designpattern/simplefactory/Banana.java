package com.eugene.designpattern.simplefactory;

/**
 * 香蕉实体类
 */
public class Banana implements IFruit {
    @Override
    public void get() {
        System.out.println("我是香蕉！");
    }
}
