package com.eugene.designpattern.simplefactory;

/**
 * 苹果实体类
 */
public class Apple implements IFruit {

    public void get() {
        System.out.println("我是苹果！");
    }

}
