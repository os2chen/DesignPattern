package com.eugene.designpattern.simplefactory;

/**
 * 水果工厂类
 */
public class FruitFactory {

    private FruitFactory() {
        throw new RuntimeException("cannot be instantiated");
    }

    public static IFruit getApple() {
        return new Apple();
    }

    public static IFruit getBanana() {
        return new Banana();
    }

    /*****************************************************************************/

    /**
     * 通过反射获取实例,添加类型时不需要修改工厂类
     *
     * @return
     */
    public static IFruit getFruit(Class<? extends IFruit> cls) {
        IFruit fruit = null;
        try {
            fruit = cls.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return fruit;
    }

}
