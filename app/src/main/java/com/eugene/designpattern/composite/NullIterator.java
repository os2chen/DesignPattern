package com.eugene.designpattern.composite;

import java.util.Iterator;

/**
 * 空迭代器，用于没有子节点的叶节点，迭代的时候可以避免判空
 */

public class NullIterator implements Iterator {
    @Override
    public boolean hasNext() {
        return false;
    }

    @Override
    public Object next() {
        return null;
    }

    @Override
    public void remove() {

    }
}
