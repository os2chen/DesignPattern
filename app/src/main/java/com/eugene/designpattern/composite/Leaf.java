package com.eugene.designpattern.composite;

/**
 * 叶节点，下面没有子节点
 */

public class Leaf extends Component {

    public Leaf(String name) {
        super(name);
    }

    @Override
    public void describe() {
        System.out.println("my name is : " + name);
    }


}
