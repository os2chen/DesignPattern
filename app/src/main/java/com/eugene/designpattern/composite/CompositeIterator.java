package com.eugene.designpattern.composite;

import java.util.Iterator;
import java.util.Stack;

/**
 * 组合迭代器，管理所有迭代器
 * 遍历整个树形结构
 */

public class CompositeIterator implements Iterator {

    /**
     * 维护一个堆栈
     */
    private Stack<Iterator> stack = new Stack<>();

    public CompositeIterator(Iterator iterator) {
        if (iterator != null) {
            stack.push(iterator);
        }
    }

    @Override
    public boolean hasNext() {
        if (stack.empty()) {
            return false;
        }
        Iterator iterator = stack.peek();
        if (!iterator.hasNext()) {
            stack.pop();
            return hasNext();
        } else {
            return true;
        }
    }

    @Override
    public Object next() {
        if (hasNext()) {
            Iterator iterator = stack.peek();
            Component component = (Component) iterator.next();
            //遍历树结构的所有节点
            stack.push(component.getIterator());
            return component;
        }
        return null;
    }
}
