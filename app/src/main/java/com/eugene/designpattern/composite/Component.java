package com.eugene.designpattern.composite;

import java.util.Iterator;

/**
 * 组合模式根节点
 */

public abstract class Component {

    protected String name;

    public Component(String name) {
        this.name = name;
    }

    /**
     * 添加子节点，有才添加
     *
     * @param child
     */
    public void addChild(Component child) {

    }

    // TODO 完成业务逻辑
    public abstract void describe();

    /**
     * 获取迭代器，对所有的子节点进行遍历
     * 下面没有子节点的时候，返回空迭代器
     */
    public Iterator getIterator() {
        return new NullIterator();
    }

}
