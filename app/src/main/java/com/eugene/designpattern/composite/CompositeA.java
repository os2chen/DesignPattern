package com.eugene.designpattern.composite;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * 子节点：下面接有子节点
 */

public class CompositeA extends Component {
    /**
     * 维护一种数据结构，管理子节点
     * 根据情况，可以选择不同的数据结构
     */
    private List<Component> list_component = new ArrayList<>();

    public CompositeA(String name) {
        super(name);
    }

    @Override
    public void addChild(Component child) {
        list_component.add(child);
    }

    @Override
    public Iterator getIterator() {
        //内部维护list，返回list的iterator
        return list_component.iterator();
    }

    @Override
    public void describe() {
        // TODO 完成业务逻辑
        System.out.println("my name is : " + name);
    }
}
