package com.eugene.designpattern.composite;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

/**
 * 子节点：下面接有子节点
 */

public class CompositeB extends Component {

    /**
     * 子节点最大数目
     */
    private int max_num = 5;

    /**
     * 子节点数目
     */
    private int compositeNum = 0;

    /**
     * 维护一个数组
     */
    private Component[] components;

    public CompositeB(String name) {
        super(name);
    }

    @Override
    public void addChild(Component child) {
        if (compositeNum >= max_num) {
            throw new IndexOutOfBoundsException("the number of children is full");
        } else {
            components[compositeNum] = child;
            compositeNum++;
        }
    }

    public boolean remove(Component component) {
        if (component == null) {
            return false;
        }
        for (int i = 0; i < components.length; i++) {
            if (component.equals(components[i])) {
                for (int j = i; j < components.length - 1; j++) {
                    components[j] = components[j++];
                }
                return true;
            }
        }
        return false;
    }

    @Override
    public Iterator getIterator() {
        return new CompositeIterator(new CompositeBIterator());
    }

    @Override
    public void describe() {
        System.out.println("my name is : " + name);
    }

    class CompositeBIterator implements Iterator {

        private int index = 0;

        @Override
        public boolean hasNext() {
            if (index < compositeNum) {
                return true;
            }
            return false;
        }

        @Override
        public Object next() {
            Component component = components[index];
            index++;
            return component;
        }
    }
}
