package com.eugene.designpattern.abstractfactory;

/**
 * 大苹果实体类
 */
public class BigApple extends Apple {
    @Override
    public void pick() {
        System.out.println("采摘大苹果");
    }
}
