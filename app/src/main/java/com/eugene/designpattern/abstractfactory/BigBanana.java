package com.eugene.designpattern.abstractfactory;

public class BigBanana extends Banana {
    @Override
    public void pick() {
        System.out.println("采摘大香蕉");
    }
}
