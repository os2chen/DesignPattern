package com.eugene.designpattern.abstractfactory;

/**
 * 小苹果实体类
 */

public class SmallApple extends Apple {
    @Override
    public void pick() {
        System.out.println("采摘小苹果");
    }
}
