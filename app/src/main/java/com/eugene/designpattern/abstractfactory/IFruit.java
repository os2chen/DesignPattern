package com.eugene.designpattern.abstractfactory;

/**
 * 水果接口
 */

public interface IFruit {

    /**
     * 采摘
     */
    void pick();

}
