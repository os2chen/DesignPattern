package com.eugene.designpattern.abstractfactory;

/**
 * 工厂接口
 * 抽象工厂模式可以向客户端提供一个接口，使得客户端在不必指定产品的具体类型的情况下，能够创建多个产品族的产品对象。
 */

public interface IFruitFactory {

    /**
     * 采摘苹果
     *
     * @return
     */
    IFruit pickApple();

    /**
     * 采摘香蕉
     *
     * @return
     */
    IFruit pickBanana();

}
