package com.eugene.designpattern.abstractfactory;


/**
 * 小香蕉实体类
 */
public class SmallBanana extends Banana {
    @Override
    public void pick() {
        System.out.println("采摘小香蕉");
    }
}
