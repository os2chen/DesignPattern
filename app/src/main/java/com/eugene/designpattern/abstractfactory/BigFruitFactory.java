package com.eugene.designpattern.abstractfactory;

/**
 * 生产大水果的工厂类
 */

public class BigFruitFactory implements IFruitFactory {

    /**
     * 生产大苹果
     * @return
     */
    @Override
    public IFruit pickApple() {
        return new BigApple();
    }

    /**
     * 生产大香蕉
     * @return
     */
    @Override
    public IFruit pickBanana() {
        return null;
    }
}
