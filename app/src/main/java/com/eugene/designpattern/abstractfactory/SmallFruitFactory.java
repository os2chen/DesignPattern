package com.eugene.designpattern.abstractfactory;

/**
 * 小水果工厂
 */

public class SmallFruitFactory implements IFruitFactory {

    /**
     * 生产小苹果
     *
     * @return
     */
    @Override
    public IFruit pickApple() {
        return new SmallApple();
    }

    /**
     * 生产小香蕉
     *
     * @return
     */
    @Override
    public IFruit pickBanana() {
        return new SmallBanana();
    }
}
