package com.eugene.designpattern.staticproxy;

/**
 * 静态代理：一个主题对应一个代理对象
 *
 * 代理类，继承主题类
 */

public class SubjectProxy extends AbstractSubject {

    //持有被代理完成主题对象的引用
    private ConcreteSubject concreteSubject;

    public SubjectProxy(ConcreteSubject concreteSubject) {
        this.concreteSubject = concreteSubject;
    }

    @Override
    public void introduce() {
        /**
         * 代理对象可以额外完成一些事情
         */

        concreteSubject.introduce();

    }
}
