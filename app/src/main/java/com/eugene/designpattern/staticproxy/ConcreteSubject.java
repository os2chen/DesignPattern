package com.eugene.designpattern.staticproxy;

/**
 * 具体主题
 */

public class ConcreteSubject extends AbstractSubject {

    @Override
    public void introduce() {
        System.out.println("我是具体主题");
    }
}
