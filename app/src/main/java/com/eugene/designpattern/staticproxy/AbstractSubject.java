package com.eugene.designpattern.staticproxy;

/**
 * 抽象主题
 */

public abstract class AbstractSubject {

    public abstract void introduce();

}
