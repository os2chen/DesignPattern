package com.eugene.designpattern.strategy;

/**
 * 定义算法接口
 */
public interface IAlgorithm {

    int calculate(int a, int b);

}
