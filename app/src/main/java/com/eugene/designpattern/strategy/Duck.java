package com.eugene.designpattern.strategy;

/**
 * 抽象鸭子
 */
public abstract class Duck {

    private IFlyBehavior flyBehavior;

    public Duck() {
    }

    public void fly() {
        if (flyBehavior != null) {
            flyBehavior.fly();
        }
    }

    /**
     * 设置具体的飞行行为
     * 添加具体的策略
     *
     * @param flyBehavior
     */
    public void setFlyBehavior(IFlyBehavior flyBehavior) {
        this.flyBehavior = flyBehavior;
    }

    public abstract void introduce();

}
