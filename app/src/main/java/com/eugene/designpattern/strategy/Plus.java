package com.eugene.designpattern.strategy;

/**
 * 加法运算策略
 */
public class Plus implements IAlgorithm {
    @Override
    public int calculate(int a, int b) {
        return a + b;
    }
}
