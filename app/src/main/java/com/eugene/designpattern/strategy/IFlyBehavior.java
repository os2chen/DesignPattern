package com.eugene.designpattern.strategy;

/**
 * 鸭子的飞行行为接口
 * 策略的抽象
 */
public interface IFlyBehavior {

    void fly();

}
