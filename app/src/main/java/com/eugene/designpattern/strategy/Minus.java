package com.eugene.designpattern.strategy;

/**
 * 减法运算策略
 */
public class Minus implements IAlgorithm {

    @Override
    public int calculate(int a, int b) {
        return a - b;
    }

}
