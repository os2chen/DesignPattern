package com.eugene.designpattern.strategy;

/**
 * 绿色的鸭子
 */

public class GreenDuck extends Duck {
    @Override
    public void introduce() {
        System.out.println("绿色的鸭子");
    }
}
