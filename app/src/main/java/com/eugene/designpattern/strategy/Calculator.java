package com.eugene.designpattern.strategy;

/**
 * 充当Context的角色
 */
public class Calculator {

    private IAlgorithm algorithm;

    /**
     * 选择不同的策略
     * @param algorithm
     */
    public void setAlgorithm(IAlgorithm algorithm) {
        this.algorithm = algorithm;
    }

    /**
     * 根据不同策略来实现不同的算法
     * @param a
     * @param b
     * @return
     */
    public int calculate(int a, int b) {
        return algorithm.calculate(a, b);
    }

}
