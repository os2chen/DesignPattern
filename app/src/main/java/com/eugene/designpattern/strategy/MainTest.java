package com.eugene.designpattern.strategy;

/**
 * 客户端调用
 */

public class MainTest {

    public static void main(String[] args) {
        Duck greenDuck = new GreenDuck();
        greenDuck.introduce();
        //设置不同的策略
        greenDuck.setFlyBehavior(new NoFlyBehavior());
        greenDuck.fly();

        Calculator calculator = new Calculator();
        //选择不同的策略
        calculator.setAlgorithm(new Minus());
        calculator.calculate(1, 2);
    }

}
