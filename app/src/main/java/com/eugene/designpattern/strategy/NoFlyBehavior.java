package com.eugene.designpattern.strategy;

/**
 * 具体策略
 */

public class NoFlyBehavior implements IFlyBehavior {
    @Override
    public void fly() {
        System.out.println("我不会飞");
    }
}
