package com.eugene.designpattern.observable;

/**
 * 具体订阅者
 */

public class ConcreteObserver implements Observer {

    @Override
    public void update() {
        System.out.println("concreteObserver update");
    }
}
