package com.eugene.designpattern.observable;

/**
 * 订阅者接口
 */

public interface Observer {

    void update();


}
