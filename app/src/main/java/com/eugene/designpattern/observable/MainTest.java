package com.eugene.designpattern.observable;

/**
 * @author chen
 */

public class MainTest {

    public static void main(String[] args) {
        ConcreteObservable observable = new ConcreteObservable();
        ConcreteObserver observer = new ConcreteObserver();
        observable.addObserver(observer);
        observable.notifyObservers();
    }

}
