package com.eugene.designpattern.observable;

import java.util.ArrayList;
import java.util.List;

/**
 * 抽象发布者
 * <p>
 * 内部维护一个集合，用于管理所有Observer
 */

public abstract class Observable {

    private final List<Observer> observerList;

    protected Observable() {
        this.observerList = new ArrayList<>();
    }

    public void addObserver(Observer observer) {
        if (observer == null)
            throw new NullPointerException();
        if (!observerList.contains(observer))
            observerList.add(observer);
    }

    public void removeObserver(Observer observer) {
        observerList.remove(observer);
    }

    public void notifyObservers() {
        for (Observer o : observerList) {
            o.update();
        }
    }

}
