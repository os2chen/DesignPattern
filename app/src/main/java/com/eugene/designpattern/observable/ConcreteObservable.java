package com.eugene.designpattern.observable;

import java.util.*;

/**
 * 具体发布者
 */

public class ConcreteObservable extends Observable {

    public void initData() {

        notifyObservers();

    }

}
