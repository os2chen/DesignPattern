package com.eugene.designpattern.command;

/**
 * 命令的具体执行者，这里可以定义一个接口
 */

public class Car {

    public void drive(){
        System.out.println("开车");
    }

    public void stop(){
        System.out.println("停车");
    }

}
