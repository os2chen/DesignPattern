package com.eugene.designpattern.command;

/**
 * 具体的命令组合
 */

public class LightOnCmd implements Command {

    private Light light;

    public LightOnCmd(Light light) {
        this.light = light;
    }

    @Override
    public void execute() {
        light.on();
    }
}
