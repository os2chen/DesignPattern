package com.eugene.designpattern.command;

/**
 * 测试
 */

public class MainTest {

    public static void main(String[] args){

        Command lightCmd = new LightOnCmd(new Light());

        Invoker<LightOnCmd> invoker = new Invoker<>();

        invoker.invoke();

    }


}
