package com.eugene.designpattern.command;

/**
 * 抽象命令
 */

public interface Command {

    /**
     * 执行具体操作的命令
     */
    void execute();

}
