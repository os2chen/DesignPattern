package com.eugene.designpattern.command;

/**
 * 命令的具体执行者，这里可以定义一个接口
 */

public class Light {

    public void on() {
        System.out.println("开灯");
    }

    public void off() {
        System.out.println("关灯");
    }


}
