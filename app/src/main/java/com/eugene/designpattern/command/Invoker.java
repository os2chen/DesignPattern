package com.eugene.designpattern.command;

/**
 * 请求者,发起命令请求
 */

public class Invoker<T extends Command> {
    private T cmd;

    public void setLightOnCmd(T cmd) {
        this.cmd = cmd;
    }

    /**
     * 真正执行命令逻辑
     */
    public void invoke() {
        cmd.execute();
    }
}
