package com.eugene.designpattern.builder;

/**
 * 抽象建造者
 */
public abstract class AbstractBuilder {

    //设置电脑品牌，品牌为默认设置
    public abstract void buildName();

    //设置主机
    public abstract void buildBoard(String board);

    //设置显示器
    public abstract void buildDisplay(String display);

    //设置操作系统，操作系统为默认设置
    public abstract void buildOS();

    //创建Computer
    public abstract AbstractComputer create();

}
