package com.eugene.designpattern.builder;

/**
 * Builder模式Java实现
 */
public class ComputerJava {

    /**
     * Computer的品牌
     */
    private String brand;

    /**
     * Computer的操作系统
     */
    private String os;

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public class Builder {

        private ComputerJava computer;

        public Builder() {
            computer = new ComputerJava();
        }

        public Builder setBrand(String name) {
            computer.setBrand(name);
            return this;
        }

        public Builder setOs(String os) {
            computer.setOs(os);
            return this;
        }

        public ComputerJava build() {
            return computer;
        }
    }

}
