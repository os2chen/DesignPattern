package com.eugene.designpattern.builder;

/**
 * 抽象计算机类
 */

public abstract class AbstractComputer {

    protected String name;
    private String board;
    private String display;
    protected String os;

    public AbstractComputer() {
    }

    /**
     * 设置电脑品牌，抽象方法表示子类必须设置
     */
    public abstract void setName();

    /**
     * 设置主板
     *
     * @param board
     */
    public void setBoard(String board) {
        this.board = board;
    }

    /**
     * 设置显示器
     *
     * @param display
     */
    public void setDisplay(String display) {
        this.display = display;
    }

    /**
     * 设置操作系统，操作系统必须有
     */
    public abstract void setOS();

    public String show() {
        return "name:{'" + name + '\'' +
                ", board='" + board + '\'' +
                ", display='" + display + '\'' +
                ", os='" + os + '\'' +
                '}';
    }
}
