package com.eugene.designpattern.builder;

/**
 * 具体要被创建的类
 */

public class MacBookComputer extends AbstractComputer {
    public MacBookComputer() {

    }

    @Override
    public void setName() {
        name = "MacBook";
    }

    @Override
    public void setOS() {
        os = "Mac OS X 10.10";
    }

}
