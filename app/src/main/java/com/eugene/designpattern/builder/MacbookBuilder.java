package com.eugene.designpattern.builder;

/**
 * MacBook创建器
 */

public class MacBookBuilder extends AbstractBuilder {
    //持有要创建对象的引用
    private AbstractComputer computer;

    public MacBookBuilder() {
        computer = new MacBookComputer();
    }

    @Override
    public void buildName() {
        computer.setName();
    }

    @Override
    public void buildBoard(String board) {
        computer.setBoard(board);
    }

    @Override
    public void buildDisplay(String display) {
        computer.setDisplay(display);
    }

    @Override
    public void buildOS() {
        computer.setOS();
    }

    @Override
    public AbstractComputer create() {
        buildName();
        buildBoard("英特尔主板");
        buildDisplay("Retina显示器");
        buildOS();
        return computer;
    }
}
