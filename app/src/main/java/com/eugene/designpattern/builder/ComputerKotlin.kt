package com.eugene.designpattern.builder

/**
 * Builder模式Kotlin实现
 */
class ComputerKotlin private constructor(builder: Builder) {

    companion object {
        fun build(init: Builder.() -> Unit) = Builder(init).build()
    }

    var brand: String?
    var os: String?

    init {
        brand = builder.brand
        os = builder.os
    }

    class Builder(init: Builder.() -> Unit) {

        var brand: String? = null
        var os: String? = null

        init {
            init()
        }

        fun setBrand(init: Builder.() -> String) = apply { brand = init() }

        fun setOs(init: Builder.() -> String) = apply { os = init() }

        fun build() = ComputerKotlin(this)

    }

}
