package com.eugene.designpattern.builder;

/**
 * 组装者
 */

public class Director {
    //持有一个构造器引用
    private AbstractBuilder builder;

    public Director(AbstractBuilder builder) {
        this.builder = builder;
    }

    /**
     * 修改构造器的接口
     *
     * @param builder
     */
    public void setBuilder(AbstractBuilder builder) {
        this.builder = builder;
    }

    public void construct() {
        AbstractComputer computer = builder.create();
        computer.show();
    }
}
