package com.eugene.designpattern.memento;

/**
 * 负责管理备忘录,不能对备忘录的内容进行操作和访问,只能将备忘录传递给其他对象
 */

public class Caretaker {

    /**
     * 备忘录对象
     */
    private Memento memento;

    /**
     * 存档
     *
     * @param memento
     */
    public void archive(Memento memento) {
        this.memento = memento;
    }

    /**
     * 获取存档
     *
     * @return
     */
    public Memento getMemento() {
        return memento;
    }


}
