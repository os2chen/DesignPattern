package com.eugene.designpattern.memento;

/**
 * 备忘录角色
 * 用于存储Originator的内部状态，并且可以防止Originator以外的对象访问Memento
 */

public class Memento {

    public int checkpoint;
    public int grade;
    public String name;


}
