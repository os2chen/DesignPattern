package com.eugene.designpattern.memento;

public class MainTest {

    public static void main(String[] args) {
        Originator originator = new Originator();
        originator.play();
        Caretaker caretaker = new Caretaker();


        //进行存档
        caretaker.archive(originator.createMemento());
        //退出游戏
        originator.quit();
        //回复游戏
        Originator newOriginator = new Originator();
        newOriginator.restore(caretaker.getMemento());


    }


}
