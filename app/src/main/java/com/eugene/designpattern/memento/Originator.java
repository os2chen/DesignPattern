package com.eugene.designpattern.memento;

/**
 * Originator 用于创建一个备忘录，可以记录和恢复自身状态
 * 同时Originator还可以根据需要决定Memento存储自身得那些状态
 */

public class Originator {

    private String name = "zhangsan";
    private int checkpoint = 1;
    private int grade = 1;

    public void play() {
        System.out.println(name + "正在第" + checkpoint + "关," + "等级为" + grade);
        grade += 10;
        checkpoint += 4;
        System.out.println(name + "正在第" + checkpoint + "关," + "等级为" + grade);
    }

    public void quit() {
        System.out.println("-------------------------");
        System.out.println("退出游戏前的属性:" + toString());
        System.out.println("退出游戏");
        System.out.println("-------------------------");
    }

    /**
     * 创建备忘录
     *
     * @return
     */
    public Memento createMemento() {
        Memento memento = new Memento();
        memento.name = name;
        memento.checkpoint = checkpoint;
        memento.grade = grade;
        return memento;
    }

    public void restore(Memento memento) {
        this.name = name;
        this.checkpoint = memento.checkpoint;
        this.grade = memento.grade;
        System.out.println("恢复游戏：" + toString());
    }

    @Override
    public String toString() {
        return "Originator{" +
                "name='" + name + '\'' +
                ", checkpoint=" + checkpoint +
                ", grade=" + grade +
                '}';
    }
}
