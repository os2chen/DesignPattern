package com.eugene.designpattern.iterator;

/**
 * 迭代器接口，定义遍历算法
 * 迭代器实现一般采用内部类
 */

public interface Iterator<T> {

    boolean hasNext();

    T next();


}
