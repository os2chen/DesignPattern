package com.eugene.designpattern.iterator;

/**
 * 数组迭代器
 */

public class ArrayIterator<T> implements Iterator<T> {

    private T[] ts;

    private int cursor = 0;

    public ArrayIterator(T[] ts) {
        this.ts = ts;
    }

    @Override
    public boolean hasNext() {
        if (cursor < ts.length) {
            return true;
        }
        return false;
    }

    @Override
    public T next() {
        if (hasNext()) {
            return ts[cursor++];
        }
        return null;
    }

}
