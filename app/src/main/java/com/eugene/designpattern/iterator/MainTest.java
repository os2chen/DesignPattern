package com.eugene.designpattern.iterator;

/**
 * 测试
 */

public class MainTest {

    public static void main(String[] args) {
        ClassA classA = new ClassA();
        classA.addStudent(new Student("小明", "123"));
        classA.addStudent(new Student("小红", "234"));
        Iterator<Student> iterator = classA.iterator();
        while (iterator.hasNext()) {
            iterator.next().introduce();
        }
    }

}
