package com.eugene.designpattern.iterator;

import java.util.ArrayList;
import java.util.List;

/**
 * 班级A
 */

public class ClassA {

    private List<Student> list;

    public ClassA() {
        list = new ArrayList<>();
    }

    /**
     * 向班级中添加学生
     *
     * @param student
     */
    public void addStudent(Student student) {
        list.add(student);
    }

    /**
     * 自我介绍
     *
     * @return
     */
    public Iterator<Student> iterator() {
        return new ListIterator<Student>(list);
    }

}
