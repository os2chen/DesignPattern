package com.eugene.designpattern.iterator;

import java.util.ArrayList;
import java.util.List;

/**
 * 遍历List
 */

public class ListIterator<T> implements Iterator {

    private List<T> list;

    /**
     * 元素指针
     */
    private int cursor = 0;

    public ListIterator(List<T> list) {
        this.list = list;
    }

    @Override
    public boolean hasNext() {
        if (cursor < list.size()) {
            return true;
        }
        return false;
    }

    @Override
    public T next() {
        if (hasNext()) {
            return list.get(cursor++);
        }
        return null;
    }

}
